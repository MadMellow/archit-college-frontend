import React from 'react';
import { connect } from 'react-redux';
import { test } from '../../actions/test'
import Intro from './intro'
import Overview from './overview'
import Courses from './courses'
import Admission from './admission'
import Placement from './placement'
import Recruiters from './recruiters'
import TopAlumni from './topAlumni'
import Gallery from './gallery'
import Facilities from './Facilities'
import Achievement from './achievements'
import Test from './test'
import Learn from './learn'
import PopularArticle from './popularArticle'
 
class College extends React.Component {
  componentDidMount() {
    this.props.test('testing Sucessful')
  }
  click = () => {
    console.log('test')
    this.props.test('testing Sucessful again')
  }
  render() {
    return (
      <div>
        <Intro data={data.intro} />
        <Overview data={data.overview} />
        <Courses data={data.CourseOffered} />
        <Admission data={data.admission_assistance} />
        <Placement data={data.placement} />
        <Recruiters data={data.recuiters} />
        <TopAlumni data={data.topAlumni} />
        <Gallery data={data.photoGallery} />
        <Facilities data={data.facilities} />
        <Achievement data={data.achievement} />
        <Test data={data.lpt.mockTest} />
        <Learn data={data.lpt.learn} />
        <Learn data={data.lpt.practice} />
        <PopularArticle data={popularArticle}/>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  text: state.testReducer,
});

const mapDispatchToProps = () => ({
  test: (val) => test(val)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps()
)(College);


export const data = {
  intro: {
    name: "Indian institute of tech, delhi",
    profileImg: "https://ingestion-content-development.s3.amazonaws.com/college-media/8c0a53a4-9826-48be-b258-33c4fbbf108d.jpeg",
    logo: "test.com",
    total_ratings: "365 ratings",
    rating: "4.5/5",
    affiliation: "Approved by UGC",
    address: "New Delhi - 180001",
    estd: "1951"
  },
  overview: "IIT Delhi offers Bachelor of Technology programs in various fields as well as dual degree B.Tech-cum-M.Tech programs. The admission to these programs is done through Joint Entrance Examination – Advanced. IIT Delhi also offers postgraduate programs awarding M.Tech (by coursework), M.S. (by research), M.Sc., M. Des., MBA under various departments and centres. The admission to M.Tech program is carried out mainly based on Graduate Aptitude Test in Engineering (GATE). M.Des ( Master of Design ) admissions are through Common Entrance Examination for Design (CEED), M.Sc. admissions are through Joint Admission Test for Masters (JAM) and MBA admissions are through Common Admission Test (CAT).",
  CourseOffered: [
    {
      name: "B.Tech.",
      imageUri: "http://s3-us-east-1.amazonaws.com/bucket/avc.jpg"
    },
    {
      name: "MBBS",
      imageUri: "http://s3-us-east-1.amazonaws.com/bucket/avc.jpg"
    },
    {
      name: "MCA",
      imageUri: "http://s3-us-east-1.amazonaws.com/bucket/avc.jpg"
    },
    {
      name: "MBA",
      imageUri: "http://s3-us-east-1.amazonaws.com/bucket/avc.jpg"
    },
    {
      name: "PGDM",
      imageUri: "http://s3-us-east-1.amazonaws.com/bucket/avc.jpg"
    },
    {
      name: "B.Sc.",
      imageUrl: "http://s3-us-east-1.amazonaws.com/bucket/avc.jpg"
    },
    {
      name: "B.Ed.",
      imageUrl: "http://s3-us-east-1.amazonaws.com/bucket/avc.jpg"
    },
    {
      name: "BCA",
      imageUrl: "http://s3-us-east-1.amazonaws.com/bucket/avc.jpg"
    },
    {
      name: "M.Tech.",
      imageUrl: "http://s3-us-east-1.amazonaws.com/bucket/avc.jpg"
    }
  ],
  admission_assistance: {
    applicationEnds: "9 dec 2020",
    testDates: "10 - 20 dec 2020",
    results: "20 jan 2020",
    selectionBrefing: "20 jun 2020"
  },
  placement: {
    average_package: "₹18.0 LPA",
    highest_package: "₹22.3 LPA",
    percentage_placement: "85.3 %"
  },
  recuiters: [
    {
      logo: "https://upload.wikimedia.org/wikipedia/commons/1/1b/Apple_logo_grey.svg"
    },
    {
      logo: "https://upload.wikimedia.org/wikipedia/commons/1/1b/Apple_logo_grey.svg"
    },
    {
      logo: "https://upload.wikimedia.org/wikipedia/commons/1/1b/Apple_logo_grey.svg"
    },
    {
      logo: "https://upload.wikimedia.org/wikipedia/commons/1/1b/Apple_logo_grey.svg"
    },
    {
      logo: "https://upload.wikimedia.org/wikipedia/commons/1/1b/Apple_logo_grey.svg"
    },
    {
      logo: "https://upload.wikimedia.org/wikipedia/commons/1/1b/Apple_logo_grey.svg"
    }
  ],
  topAlumni: [
    {
      description: "'Pichai Sundararajan, also known as Sundar Pichai, is an Indian American business executive, the CEO of Alphabet Inc. and its subsidiary Google LLC.'",
      name: "Sundar Pichai",
      media_url: "https://ingestion-content-development.s3.amazonaws.com/college-media/8c0a53a4-9826-48be-b258-33c4fbbf108d.jpeg"
    },
    {
      description: "'Pichai Sundararajan, also known as Sundar Pichai, is an Indian American business executive, the CEO of Alphabet Inc. and its subsidiary Google LLC.'",
      name: "Sundar Pichai",
      media_url: "https://ingestion-content-development.s3.amazonaws.com/college-media/8c0a53a4-9826-48be-b258-33c4fbbf108d.jpeg"
    },
    {
      description: "'Pichai Sundararajan, also known as Sundar Pichai, is an Indian American business executive, the CEO of Alphabet Inc. and its subsidiary Google LLC.'",
      name: "Sundar Pichai",
      media_url: "https://ingestion-content-development.s3.amazonaws.com/college-media/8c0a53a4-9826-48be-b258-33c4fbbf108d.jpeg"
    }
  ],
  photoGallery: [
    {
      name: "classroom",
      media_url: "https://ingestion-content-development.s3.amazonaws.com/college-media/8c0a53a4-9826-48be-b258-33c4fbbf108d.jpeg"
    },
    {
      name: "classroom",
      media_url: "https://ingestion-content-development.s3.amazonaws.com/college-media/8c0a53a4-9826-48be-b258-33c4fbbf108d.jpeg"
    },
    {
      name: "classroom",
      media_url: "https://ingestion-content-development.s3.amazonaws.com/college-media/8c0a53a4-9826-48be-b258-33c4fbbf108d.jpeg"
    },
    {
      name: "classroom",
      media_url: "https://ingestion-content-development.s3.amazonaws.com/college-media/8c0a53a4-9826-48be-b258-33c4fbbf108d.jpeg"
    }
  ],
  facilities: [
    {
      name: "classroom",
      media_url: "https://ingestion-content-development.s3.amazonaws.com/college-media/8c0a53a4-9826-48be-b258-33c4fbbf108d.jpeg"
    },
    {
      name: "classroom",
      media_url: "https://ingestion-content-development.s3.amazonaws.com/college-media/8c0a53a4-9826-48be-b258-33c4fbbf108d.jpeg"
    },
    {
      name: "classroom",
      media_url: "https://ingestion-content-development.s3.amazonaws.com/college-media/8c0a53a4-9826-48be-b258-33c4fbbf108d.jpeg"
    },
    {
      name: "classroom",
      media_url: "https://ingestion-content-development.s3.amazonaws.com/college-media/8c0a53a4-9826-48be-b258-33c4fbbf108d.jpeg"
    },
    {
      name: "classroom",
      media_url: "https://ingestion-content-development.s3.amazonaws.com/college-media/8c0a53a4-9826-48be-b258-33c4fbbf108d.jpeg"
    },
    {
      name: "classroom",
      media_url: "https://ingestion-content-development.s3.amazonaws.com/college-media/8c0a53a4-9826-48be-b258-33c4fbbf108d.jpeg"
    }
  ],
  achievement: [
    {
      award: "Rank 210 in QS world uni .......",
      awardee: "QS World",
      date: "Dec -2019",
      desc: "QS World University Rankings is an annual publication of university rankings by Quacquarelli Symonds."
    },
    {
      award: "Rank 210 in QS world uni .......",
      awardee: "QS World",
      date: "Dec -2019",
      desc: "QS World University Rankings is an annual publication of university rankings by Quacquarelli Symonds."
    },
    {
      award: "Rank 210 in QS world uni .......",
      awardee: "QS World",
      date: "Dec -2019",
      desc: "QS World University Rankings is an annual publication of university rankings by Quacquarelli Symonds."
    }
  ],
  lpt: {
    mockTest: {
      name: "JEE Mock Test",
      links: [
        "link1",
        "link2",
        "link3"
      ]
    },
    learn: {
      name: "Learn JEE Concepts",
      links: [
        "https://youtu.be/YykjpeuMNEk",
        "https://youtu.be/YykjpeuMNEk",
        "https://youtu.be/YykjpeuMNEk"
      ]
    },
    practice: {
      name: "Preparing for JEE",
      links: [
        "https://youtu.be/YykjpeuMNEk",
        "https://youtu.be/YykjpeuMNEk",
        "https://youtu.be/YykjpeuMNEk"
      ]
    }
  },
  Cards: [
    {
      collegePridictor: {
        title: "where will i get admission",
        desc: "something ....",
        btnName: " get started",
        btnLink: "test.com"
      },
      guidedPractice: {
        title: "where will i get admission",
        desc: "something ....",
        btnName: " get started",
        btnLink: "test.com"
      },
      achieve: {
        title: "where will i get admission",
        desc: "something ....",
        btnName: " get started",
        btnLink: "test.com"
      }
    }
  ],
  everything: [
    {
      name: "jee mains",
      fields: [
        {
          overview: [
            "link1",
            "link2"
          ]
        },
        {
          exam: [
            "link1",
            "link2"
          ]
        },
        {
          result: [
            "link1",
            "link2"
          ]
        }
      ]
    },
    {
      name: "neet",
      fields: [
        {
          overview: [
            "link1",
            "link2"
          ]
        },
        {
          exam: [
            "link1",
            "link2"
          ]
        },
        {
          result: [
            "link1",
            "link2"
          ]
        }
      ]
    }
  ]
}

export const popularArticle = [
  {
    title: "Jee mains 2020 application form",
    img: "https://ingestion-content-development.s3.amazonaws.com/college-media/8c0a53a4-9826-48be-b258-33c4fbbf108d.jpeg",
    desc: "dometing aboit it......",
    date: "15 mar 2020"
  },
  {
    title: "Jee mains 2020 application form",
    img: "https://ingestion-content-development.s3.amazonaws.com/college-media/8c0a53a4-9826-48be-b258-33c4fbbf108d.jpeg",
    desc: "dometing aboit it......",
    date: "15 mar 2020"
  },
  {
    title: "Jee mains 2020 application form",
    img: "https://ingestion-content-development.s3.amazonaws.com/college-media/8c0a53a4-9826-48be-b258-33c4fbbf108d.jpeg",
    desc: "dometing aboit it......",
    date: "15 mar 2020"
  }
]