import React from 'react';
import './index.scss';

const Overview = props => {
    return (
        <div className="div-overview">
            <p>{props.data}</p>
        </div>
    )
};

export default Overview;