import React from 'react';
import './index.scss';
import CardMedia from '../../../components/cardMedia'

const Courses = props => {
    return (
        <div className="div-gallery">
            <h1>Photo Gallery</h1>
            <div style ={{overflowX: "scroll", overflowY: "hidden", whiteSpace: "nowrap"}}>
                {props.data.map((img) => {
                    return <CardMedia imgLink={img.media_url} name={img.name}/>
                })}
            </div>
        </div>
    )
};

export default Courses;