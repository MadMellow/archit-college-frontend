import React from 'react';
import './index.scss';

const Courses = props => {
    return (
        <div className="div-alumni">
            <h1>Top Alumni</h1>
            <div>
                {props.data.map((alumni) => {
                    return (<div className="alumni">
                        <div className="alumniName">
                            <img
                                style={{borderRadius: '10px', width:'16%'}}
                                src={alumni.media_url}
                                alt=''
                            />
                            <p>{alumni.name}</p>
                        </div>
                        <div className="alumniDesc">
                            <p>{alumni.description}</p>
                        </div>
                    </div>)
                })}
            </div>
        </div>
    )
};

export default Courses;