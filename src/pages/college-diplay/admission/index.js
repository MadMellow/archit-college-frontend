import React from 'react';
import './index.scss';

const Admission = props => {
    return (
        <div className="div-admission">
            <h1>Admission Assistance</h1>
            <div>
                <p className="key">Application Ends:</p>
                <p className="val">{props.data.applicationEnds}</p>
            </div>
            <hr/>
            <div>
                <p className="key">Test Dates:</p>
                <p className="val">{props.data.testDates}</p>
            </div>
            <hr/>
            <div>
                <p className="key">Results:</p>
                <p className="val">{props.data.results}</p>
            </div>
            <hr/>
            <div style={{paddingBottom:'20px'}}>
                <p className="key">Selection Brefing:</p>
                <p className="val">{props.data.selectionBrefing}</p>
            </div>
        </div>
    )
};

export default Admission;