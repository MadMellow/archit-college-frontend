import React from 'react';
import './index.scss';
import CardMedia from '../../../components/cardMedia'

const Learn = props => {
    return (
        <div className="div-learn">
            <h1>{props.data.name}</h1>
            <div style ={{overflowX: "scroll", overflowY: "hidden", whiteSpace: "nowrap"}}>
                {props.data.links.map((item) => {
                    return <CardMedia videoLink = {item} />
                })}
            </div>
        </div>
    )
};

export default Learn;