import React from 'react';
import Styled from 'styled-components';

const CardWrapper = Styled.div`
    background-color: #FFFFFF;
    border-radius: 10px;
    box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.12);
    font-family: Roboto;
	height: 100pt;
	position: relative;
    width: 28%;
    margin: auto auto 12px auto;
`;

const CardTitle = Styled.div`
    color: white;
	font-size: 20px;
	bottom: 10px;
	margin-left:10%;
	position:absolute;
    font-family: Roboto;
`;
const DefaultText = Styled.div`
text-align: center;
display: flex;
justify-content: center;
align-items: center;
height: 100%;
`;

const CardSmall = props => {
	console.log(props)
	return (
		<CardWrapper
			style={{ ...props.propStyle }}
		>
			{props.hasOwnProperty('cardImg') ? (
				props.cardImg.length !== 0 ? (
					<img
						style={{position: 'absolute', borderRadius: '10pt'}}
						src={props.hasOwnProperty('cardImg') ? props.cardImg : 'test'}
						alt=''
						height='100%'
						width='100%'
					/>
				) : (
					<div style={{ backgroundColor: '#F9F2EB', height: '240px' }}>
						<DefaultText>No image</DefaultText>
					</div>
				)
			) : null}
			{props.hasOwnProperty('logo') ? (
				props.logo.length !== 0 ? (
					<img
						style={{position: 'absolute', borderRadius: '8pt', top:'20%', left:'20%'}}
						src={props.hasOwnProperty('logo') ? props.logo : 'test'}
						alt=''
						height='60%'
						width='60%'
					/>
				) : (
					<div style={{ backgroundColor: '#F9F2EB', height: '240px' }}>
						<DefaultText>No image</DefaultText>
					</div>
				)
			) : null}
			{props.hasOwnProperty('text') ? (
				<CardTitle>
					{props.text}
				</CardTitle>
			) : null}
			{props.hasOwnProperty('placement') ? (
				<div style={{display:"block"}}>
					<p style={{fontSize:"20px", textAlign:"center"}}>{props.placement[0]}</p>
					<p style={{fontSize:"20px", fontWeight:'bold', textAlign:"center"}}>{props.placement[1]}</p>
				</div>
			) : null}
			{props.hasOwnProperty('mockTest') ? (
				<div style={{display:"block"}}>
					<p style={{fontSize:"20px", fontWeight:'bold', textAlign:"center", lineHeight:"24pt", wordSpacing:'100vw'}}>{props.mockTest[0]}</p>
				</div>
			) : null}
		</CardWrapper>
	);
};

export default CardSmall;
