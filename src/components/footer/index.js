import React from 'react';

import './index.scss';

const Footer1 = (props) => {

    return (
        <div className="footer-one">
            
                {props.data2.fields.map((item) => {
                    return (
                        <div>
                            <p style={{fontSize:'16pt'}}>{item.title} 
                            <div id='button'>
                                <button> x </button>
                            </div></p>
                    
                        </div>
                        
                    )
                })}
                
           
        </div>
    )
};

export default Footer1;