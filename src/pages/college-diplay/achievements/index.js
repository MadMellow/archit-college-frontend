import React from 'react';
import './index.scss';

const Achievements = props => {
    return (
        <div className="div-achievement">
            <h1>Achievements</h1>
            <div className="achievementContainer">
                {props.data.map((item) => {
                    return (
                        <div>
                            <p style={{fontSize:'16pt'}}>{item.award}</p>
                            <p style={{fontSize:'12pt'}}>{item.date} | by {item.awardee}</p>
                            <p style={{fontSize:'14pt', color:'#353A3C'}}>{item.desc}</p>
                        </div>
                    )
                })}
            </div>
        </div>
    )
};

export default Achievements;