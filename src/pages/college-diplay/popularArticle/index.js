import React from 'react';
import './index.scss';
import CardArticle from '../../../components/cardArticle'

const Achievements = props => {
    return (
        <div className="div-popular">
            <h1>Popular Articles</h1>
            {props.data.map((article) => {
                return <CardArticle title={article.title} desc={article.desc} date={article.date}
                    link={article.link} cardImg={article.img} />
            })}
        </div>
    )
};

export default Achievements;