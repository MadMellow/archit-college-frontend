import { combineReducers } from 'redux'
import testReducer from './test'
import auth from './test2'

const rootReducer = combineReducers({
    testReducer,
    auth
});

export default rootReducer;