import React from 'react';
import './index.scss';
import CardSmall from '../../../components/cardSmall'

const Courses = props => {
    return (
        <div className="div-placement">
            <h1>Placement Details</h1>
            <div>
                <CardSmall  placement = {['Highest Package', props.data.highest_package]} propStyle={{ backgroundColor: '#FFBF7A' }} />
                <CardSmall  placement = {['Average Package', props.data.average_package]} propStyle={{ backgroundColor: '#FFB697' }} />
                <CardSmall  placement = {['Placement Percentage', props.data.percentage_placement]} propStyle={{ backgroundColor: '#C2F6FF' }} />
            </div>
        </div>
    )
};

export default Courses;