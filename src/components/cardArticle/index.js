import React from 'react';
import Styled from 'styled-components';
import shareIcon from '../../assets/share.png';

const CardWrapper = Styled.div`
    background-color: #FFFFFF;
    border-radius: 8px 8px 8px 8px;
    box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.12);
    font-family: Roboto;
	position: relative;
    width: 90%;
    margin: auto auto 12px auto;
`;

const CardTitle = Styled.div`
    color: black;
	font-size:20px;
	padding:10px;
	font-family: Roboto;
	font-weight: bold;
`;

const CardDesc = Styled.div`
    color: black;
	font-size:18px;
	padding:10px;
    font-family: Roboto;
`;

const CardDate = Styled.div`
    color: #353A3C;
	font-size:18px;
	padding:10px;
    font-family: Roboto;
`;

const DefaultText = Styled.div`
text-align: center;
justify-content: center;
align-items: center;
height: 100%;
`;

const CardArticle = props => {
	console.log(props)
	return (
		<CardWrapper
			style={{ ...props.propStyle }}
		>
			{props.hasOwnProperty('cardImg') ? (
				props.cardImg.length !== 0 ? (
					<img
						style={{ borderRadius: '8px 8px' }}
						src={props.hasOwnProperty('cardImg') ? props.cardImg : 'test'}
						alt=''
						height='150pt'
						width='100%'
					/>
				) : (
						<div style={{ backgroundColor: '#F9F2EB', height: '240px' }}>
							<DefaultText>No image</DefaultText>
						</div>
					)
			) : null}
			{props.hasOwnProperty('title') ? (
				<CardTitle>
					{props.title}
				</CardTitle>
			) : null}
			{props.hasOwnProperty('desc') ? (
				<CardDesc>
					{props.desc}
				</CardDesc>
			) : null}
			{props.hasOwnProperty('date') ? (
				<CardDate>
					{props.date}
				</CardDate>
			) : null}
			<img src={shareIcon} alt='' style={{
				height: '15px', width: '15px', margin: 'auto',
				position: 'absolute', bottom: '10px', right: '15px'
			}} />
		</CardWrapper>
	);
};

export default CardArticle;
