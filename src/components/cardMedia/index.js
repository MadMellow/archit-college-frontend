import React from 'react';
import Styled from 'styled-components';
import ReactPlayer from 'react-player';

const CardWrapper = Styled.div`
    border-radius: 8px 8px 8px 8px;
    font-family: Roboto;
	height: 180pt;
    position: relative;
    margin:15px;
    width: 70%;
    display: inline-block;
    margin: 10px;
`;
const CardTitle = Styled.div`
    color: white;
	font-size: 25pt;
	bottom: 10px;
	margin-left:5%;
	position:absolute;
    font-family: Roboto;
`;

const CardVideo = props => {
    return (
        <CardWrapper
            style={{ ...props.propStyle }}
        >
            {props.hasOwnProperty('videoLink') ? (
                <ReactPlayer controls width='100%' height='100%' url={props.videoLink} ></ReactPlayer>
            ) : null}
            {props.hasOwnProperty('imgLink') ? (
                <img
                style={{width:'100%', height:'100%'}}
                src={props.imgLink}
                alt=''
            />
            ) : null}
            {props.hasOwnProperty('imgLink') ? (
                <CardTitle>{props.name}</CardTitle>
            ) : null}
            
            
        </CardWrapper>
    );
};

export default CardVideo;
