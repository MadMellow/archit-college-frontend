import React from 'react';
import './index.scss';
import CardSmall from '../../../components/cardSmall'

const Facilities = props => {
    return (
        <div className="div-courses">
            <h1>Facilities</h1>
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {props.data.map((item) => {
                     return(<CardSmall text={item.name} cardImg={item.media_url} propStyle={{height:'130pt'}}/>)
                })}
            </div>
        </div>
    )
};

export default Facilities;