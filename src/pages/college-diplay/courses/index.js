import React from 'react';
import './index.scss';
import CardSmall from '../../../components/cardSmall'

const Courses = props => {
    return (
        <div className="div-courses">
            <h1>Courses Offered</h1>
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {props.data.map((course) => {
                     return(<CardSmall text={course.name} propStyle={{backgroundColor: '#2B196A', height:'120pt'}}/>)
                })}
            </div>
        </div>
    )
};

export default Courses;