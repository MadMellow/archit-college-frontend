import React from 'react';
import './index.scss';
import CardSmall from '../../../components/cardSmall'

const Courses = props => {
    return (
        <div className="div-test">
            <h1>{props.data.name}</h1>
            <div>
                <CardSmall  mockTest = {['Mock Test-01', props.data.links[0]]} propStyle={{ backgroundColor: '#FFBF7A' }} />
                <CardSmall  mockTest = {['Mock Test-02', props.data.links[1]]} propStyle={{ backgroundColor: '#FFB697' }} />
                <CardSmall  mockTest = {['Mock Test-03', props.data.links[2]]} propStyle={{ backgroundColor: '#C2F6FF' }} />
            </div>
        </div>
    )
};

export default Courses;