import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import College from './pages/college-diplay'
// import { useSelector } from 'react-redux'
const PageNotFound = () => <h1>Page Not Available</h1>;
const healthCheck = () => <h1>The App is Healthy</h1>;

function App() {
  // const auth = useSelector(store => store.auth)
  return (
    <Router>
      <Switch>
        <Route exact path='/college' component={College} />
        <Route exact path='/college-frontend/v1/health' component={healthCheck} />
        <Route component={PageNotFound} />
      </Switch>
    </Router>
  );
}

export default App;
