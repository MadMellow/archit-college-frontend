import React from 'react';
import './index.scss';
import Logo from '../../../assets/logo.png'

const Intro = props => {
    return (
        <div className="div-intro">
            <div style={{backgroundImage: 'linear-gradient(white 50%, #2B196A 120%)'}}>
                <img
                    style={{  borderRadius: '12pt', width: '100%'}}
                    src={props.data.profileImg}
                    alt=''
                />
                <img src={Logo} alt='' style={{
                    height: '35px', width: '35px',
                }} />
                <h1 className="intro-clg-name">{props.data.name}</h1>
            </div>
            <div>
                <h2 style={{color:'white'}}>{props.data.address}</h2>
            </div>
        </div>
    )
};

export default Intro;