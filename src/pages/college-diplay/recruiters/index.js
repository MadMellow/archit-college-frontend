import React from 'react';
import './index.scss';
import CardSmall from '../../../components/cardSmall'

const Courses = props => {
    return (
        <div className="div-recuiters">
            <h1>Recuiters</h1>
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {props.data.map((recuiter) => {
                    return (<CardSmall logo= {recuiter.logo} propStyle={{
                        backgroundColor: '#2B196A',
                        boxShadow: '0 9pt 33pt 7pt rgb(247 247 247 / 11%)'
                 }}/>)
                })}
            </div>
        </div>
    )
};

export default Courses;